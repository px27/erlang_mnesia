%%%-------------------------------------------------------------------
%%% @author Piotr Wąchała
%%% @copyright (C) 2015,
%%% @doc
%%%
%%% @end
%%% Created : 23. sty 2015 23:23
%%%-------------------------------------------------------------------
-author("Piotr").

-record(client, {clientId, firstName, lastName, postalCode, sex, phone}).
-record(car, {carId, make, model, year, mileage}).
-record(rent, {carId, clientId}).
