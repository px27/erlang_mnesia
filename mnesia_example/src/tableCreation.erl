%%%-------------------------------------------------------------------
%%% @author Piotr Wąchała
%%% @copyright (C) 2015,
%%% @doc
%%%
%%% @end
%%% Created : 23. sty 2015 23:26
%%%-------------------------------------------------------------------
-module(tableCreation).
-author("Piotr Wąchała").

%% API
-export([init/0, selectAllFemales/0, selectAllCars/0, addToMileage/1]).
-export([test/0]).

-include_lib("stdlib/include/qlc.hrl").
-include("records.hrl").

init() ->
  mnesia:create_schema([node()]),
  mnesia:start(),
  mnesia:create_table(client,
    [{attributes, record_info(fields, client)}]),

  mnesia:create_table(rent, [{type, bag},
    {attributes, record_info(fields, rent)}]),

  mnesia:create_table(car,
    [{attributes, record_info(fields, car)}]).


test() ->
  insertIntoCars([#car{carId = 1, make = "Honda",model = "Civic", year = 2010, mileage = 120000},
    #car{carId = 2, make = "Ford", model = "F-150", year = 2012, mileage = 86000},
    #car{carId = 3, make = "Dodge", model = "RAM 1500", year = 2008, mileage = 180000},
    #car{carId = 4, make = "Ford", model = "Crown Victoria", year = 2000, mileage = 195000},
    #car{carId = 5, make = "Cadillac", model = "Escalade", year = 2009, mileage = 1850000},
    #car{carId = 6, make = "Chevrolet", model = "Silverado", year = 2012, mileage = 140000}]),

  addClient(#client{clientId = 1, firstName = "Piotr", lastName = "Wachala", postalCode = "34-600",sex = male, phone = "123456789"},[1,2,3]),
  addClient(#client{clientId = 2, firstName = "John", lastName = "Brown", postalCode = "12323", sex = male, phone = "12344512"}, [2,3,4]),
  addClient(#client{clientId = 3, firstName = "Emily", lastName = "Smith", postalCode = "4345", sex = female, phone = "98743423"}, [5,2]),
  addClient(#client{clientId = 4, firstName = "Gemma", lastName = "Cortez", postalCode = "4566", sex = female, phone = "213312321"}, [3,3]).

addClient(Person, Rents) ->
  Fun = fun() ->
    mnesia:write(Person),
    ClientId = Person#client.clientId,
    addRents(ClientId, Rents)
  end,
  mnesia:transaction(Fun).

addRents(ClientId, [Head | Tail]) ->
  mnesia:write(#rent{clientId = ClientId, carId = Head}),
  addRents(ClientId, Tail);
addRents(_ , []) -> ok.

insertIntoCars(Cars) ->
  Fun = fun() ->
    addCars(Cars)
  end,
  mnesia:transaction(Fun).

addCars([Head|Tail]) ->
  mnesia:write(Head),
  addCars(Tail);
addCars([]) -> ok.

selectAllFemales() ->
  F = fun() ->
    Q = qlc:q([{E#client.firstName,E#client.lastName} || E <- mnesia:table(client),
      E#client.sex == female]),
    qlc:e(Q)
  end,
  mnesia:transaction(F).

selectAllCars() ->
  F = fun() ->
    Cars = #car{make = '$1', model = '$2', year = '$3', mileage = '$4', _ = '_'},
    Guard = {'>', '$4', 150000},
    Result = '$$',
    mnesia:select(car, [{Cars,[Guard],[Result]}])
  end,
  mnesia:transaction(F).

addToMileage(Amount) ->
  F = fun() ->
    Q = qlc:q([E || E <- mnesia:table(car)]),
    CarList = qlc:e(Q),
    modifyMileage(CarList, Amount)
  end,
  mnesia:transaction(F).

modifyMileage([Car |Tail], Amount) ->
  NewMileage = Car#car.mileage + Amount,
  New = Car#car{mileage = NewMileage},
  mnesia:write(New),
  modifyMileage(Tail, Amount);
modifyMileage([], _) -> 0.



