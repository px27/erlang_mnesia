-module(mnesiaExample).

-include_lib("stdlib/include/qlc.hrl").
-include("company.hrl").

-export([init/0, insert_emp/3, raise/2, all_females/0, females/0, raise_females/1]).

init() ->
  mnesia:create_table(employee,
    [{attributes, record_info(fields, employee)}]),
  mnesia:create_table(dept,
    [{attributes, record_info(fields, dept)}]),
  mnesia:create_table(project,
    [{attributes, record_info(fields, project)}]),
  mnesia:create_table(manager, [{type, bag},
    {attributes, record_info(fields, manager)}]),
  mnesia:create_table(at_dep,
    [{attributes, record_info(fields, at_dep)}]),
  mnesia:create_table(in_proj, [{type, bag},
    {attributes, record_info(fields, in_proj)}]).


insert_emp(Emp, DeptId, ProjNames) ->
  Ename = Emp#employee.name,
  Fun = fun() ->
    mnesia:write(Emp),
    AtDep = #at_dep{emp = Ename, dept_id = DeptId},
    mnesia:write(AtDep),
    mk_projs(Ename, ProjNames)
  end,
  mnesia:transaction(Fun).

mk_projs(Ename, [ProjName|Tail]) ->
  mnesia:write(#in_proj{emp = Ename, proj_name = ProjName}),
  mk_projs(Ename, Tail);
mk_projs(_, []) -> ok.

raise(Eno, Raise) ->
  F = fun() ->
    [E] = mnesia:read(employee, Eno, write),
    Salary = E#employee.salary + Raise,
    New = E#employee{salary = Salary},
    mnesia:write(New)
  end,
  mnesia:transaction(F).

all_females() ->
  F = fun() ->
    Female = #employee{sex = female, name = '$1', _ = '_'},
    mnesia:select(employee, [{Female, [], ['$1']}])
  end,
  mnesia:transaction(F).

females() ->
  F = fun() ->
    Q = qlc:q([E#employee.name || E <- mnesia:table(employee),
      E#employee.sex == female]),
    qlc:e(Q)
  end,
  mnesia:transaction(F).

raise_females(Amount) ->
  F = fun() ->
    Q = qlc:q([E || E <- mnesia:table(employee),
      E#employee.sex == female]),
    Fs = qlc:e(Q),
    over_write(Fs, Amount)
  end,
  mnesia:transaction(F).

over_write([E|Tail], Amount) ->
  Salary = E#employee.salary + Amount,
  New = E#employee{salary = Salary},
  mnesia:write(New),
  1 + over_write(Tail, Amount);
over_write([], _) ->
  0.
